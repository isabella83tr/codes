#!/usr/bin/env python
# coding: utf-8


import pydicom as dicom
import numpy as np
import os
from PIL import Image
import pandas as pd
import csv
import sys



def read_xray(path):
    
    dcm = dicom.read_file(path)    
    data = dcm.pixel_array    
    img_from_arr = Image.fromarray(data)
        
    return data, img_from_arr

